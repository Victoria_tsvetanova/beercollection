//
//  FavouritesTableViewCell.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 17.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class FavouritesTableViewCell: UITableViewCell {
    
    var beer: Beer?
    
    var didPressLike: Bool? = false
    
    var didPressMore: ((Beer) -> Void)?
    
    var didPressPreview: ((Beer) -> Void)?
    
    var didPressRemove: ((IndexPath) -> Void)?
    
    var isLiked: Bool?
    
    @IBOutlet weak var beerImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    var popUpVC : PopUpViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setValues(beer : Beer) {
        
        let font = TextFont()
        
        self.beer = beer
        nameLabel.text = beer.name
        nameLabel.font = font.getTitleFont()
        
        dateLabel.text = beer.date
        dateLabel.font = font.getTextFont()
        
        isLiked = UserDefaults.standard.object(forKey: nameLabel.text!) as? Bool
        
        DispatchQueue.main.async {
            if let imageURL = beer.imageURL,
                let url = URL(string: imageURL) {
                if let data = try? Data(contentsOf: url) {
                    self.beerImageView.image = UIImage(data: data)
                }
            }
        }
        
    }
    
    // MARK: - Press buttons
    
    @IBAction func popUpButton(_ sender: Any) {
        guard let beer = beer else { return }
        didPressPreview?(beer)
        
    }
    
    @IBAction func showBeerDetails(_ sender: Any) {
        guard let beer = beer else { return }
        didPressMore?(beer)
        
    }

  
}
