//
//  BeerTableViewCell.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 15.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import CoreData

class BeerTableViewCell : UITableViewCell {
    
    var didPressMore: ((Beer) -> Void)?
    
    var didPressPreview: ((Beer) -> Void)?
    
    var didPressLike: ((BeerTableViewCell) -> Void)?
    
    var didPressRemove: ((IndexPath) -> Void)?
    
    var beer: Beer?
    
     var isLiked : Bool?
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var imageVew: UIImageView!
    
    @IBOutlet var likeImage: UIButton!
    
    @IBAction func likeButton(_ sender: Any?) {
        didPressLike?(self)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setValues(beer : Beer) {
        
        let font = TextFont()
    
        self.beer = beer
        nameLabel.text = beer.name
        nameLabel.font = font.getTitleFont()
        
        nameLabel.minimumScaleFactor = 15/UIFont.labelFontSize
        nameLabel.adjustsFontSizeToFitWidth = true
    
        nameLabel.adjustsFontForContentSizeCategory = true
        
        dateLabel.text = beer.date
        dateLabel.font = font.getTextFont()
        
        isLiked = UserDefaults.standard.object(forKey: nameLabel.text!) as? Bool ?? false
        
        if isLiked != nil {
            if isLiked! {
                let image1 = UIImage(named: "heart_full_icon")
                likeImage.setImage(image1, for: .normal)
            } else {
                let image1 = UIImage(named: "heart_empty_icon")
                likeImage.setImage(image1, for: .normal)
            }
        }
        
        DispatchQueue.main.async {
            if let imageURL = beer.imageURL,
                let url = URL(string: imageURL) {
                if let data = try? Data(contentsOf: url) {
                    self.imageVew.image = UIImage(data: data)
                }
            }
        }
        
        
    }
    
    //MARK: - Press buttons
    
    @IBAction func showPopUp(_ sender: Any?) {
        guard let beer = beer else { return }
        didPressPreview?(beer)
        
    }
    
    @IBAction func showBeerDetails(_ sender: Any) {
        guard let beer = beer else { return }
        didPressMore?(beer)
        
    }
    

}
