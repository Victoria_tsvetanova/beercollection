//
//  FavouritesCollectionViewCell.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 8.05.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class FavouritesCollectionViewCell: UICollectionViewCell {
    
    var beer: Beer?
    
    var isLiked : Bool? = false
    
    var popUpVC : PopUpViewController!
    
    var didPressMore: ((Beer) -> Void)?
    
    var didPressLike: ((BeerCollectionViewCell) -> Void)?
    
    var didPressPreview: ((Beer) -> Void)?
    
    var didPressRemove: ((IndexPath) -> Void)?
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var likeImage: UIButton!
    
    func setValues(beer : Beer) {
        
        let font = TextFont()
        
        self.beer = beer
        nameLabel.text = beer.name
        nameLabel.font = font.getTitleFont()
        
        dateLabel.text = beer.date
        dateLabel.font = font.getTextFont()
        
        DispatchQueue.main.async {
            if let imageURL = beer.imageURL,
                let url = URL(string: imageURL) {
                if let data = try? Data(contentsOf: url) {
                    self.imageView.image = UIImage(data: data)
                }
            }
        }
        
        
    }
    
    //MARK: - Press buttons
    
    @IBAction func showPopUp(_ sender: Any?) {
        guard let beer = beer else { return }
        didPressPreview?(beer)
        
    }
    
    @IBAction func showBeerDetails(_ sender: Any) {
        guard let beer = beer else { return }
        didPressMore?(beer)
        
    }
    
}
