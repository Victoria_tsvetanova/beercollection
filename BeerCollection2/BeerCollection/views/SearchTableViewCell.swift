//
//  SearchTableViewCell.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 19.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet var searchTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setValue(text : String) {
        searchTextLabel.text = text
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
