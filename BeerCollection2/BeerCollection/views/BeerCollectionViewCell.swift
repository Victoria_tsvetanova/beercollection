//
//  BeerCollectionViewCell.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 16.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class BeerCollectionViewCell: UICollectionViewCell {
    
    var beer: Beer?
    
    var isLiked : Bool? = false
    
    var popUpVC : PopUpViewController!
    
    var didPressMore: ((Beer) -> Void)?
    
    var didPressLike: ((BeerCollectionViewCell) -> Void)?
    
    var didPressPreview: ((Beer) -> Void)?
    
    var didPressRemove: ((IndexPath) -> Void)?
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var likeImage: UIButton!
    
    @IBAction func likeButton(_ sender: Any?) {
        didPressLike?(self)
        
    }
    
    func setValues(beer : Beer) {
        
        let font = TextFont()
        
        self.beer = beer
        nameLabel.text = beer.name
        nameLabel.font = font.getTitleFont()
        
        dateLabel.text = beer.date
        dateLabel.font = font.getTextFont()
        
        isLiked = UserDefaults.standard.object(forKey: nameLabel.text!) as? Bool ?? false

        if isLiked != nil {
            if isLiked! {
                let image1 = UIImage(named: "heart_full_icon")
                likeImage.setImage(image1, for: .normal)
            } else {
                let image1 = UIImage(named: "heart_empty_icon")
                likeImage.setImage(image1, for: .normal)
            }
        }
        
        DispatchQueue.main.async {
            if let imageURL = beer.imageURL,
            let url = URL(string: imageURL) {
                if let data = try? Data(contentsOf: url) {
                    self.imageView.image = UIImage(data: data)
                }
            }
        }
        
        
    }
    
    //MARK: - Press buttons
    
    @IBAction func showPopUp(_ sender: Any?) {
        guard let beer = beer else { return }
        didPressPreview?(beer)
        
    }
    
    @IBAction func showBeerDetails(_ sender: Any) {
        guard let beer = beer else { return }
        didPressMore?(beer)
        
    }
}
