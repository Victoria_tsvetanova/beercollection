//
//  Hop.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 11.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import Foundation

class Amount: Codable {
    
    var value: Double?
}

class Hop: Codable {
    
    var name: String?
    var amount: Amount?
}

class Ingredients: Codable {
    
    var hops: Array<Hop?>
    
}

