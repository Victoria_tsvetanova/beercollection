//
//  BeerModel.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 11.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import Foundation

class Beer: Codable {

    var name: String?
    var description: String?
    var date: String?
    var imageURL: String?
    var isLiked: Bool? = false
    var ingredients: Ingredients?
    var foodPairing: Array<String>?

    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case date = "first_brewed"
        case imageURL = "image_url"
        case ingredients
        case foodPairing = "food_pairing"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        description = try container.decode(String.self, forKey: .description)
        date = try container.decode(String.self, forKey: .date)
        imageURL = try container.decode(String.self, forKey: .imageURL)
        foodPairing = try container.decode(Array<String>.self, forKey: .foodPairing)
        ingredients = try container.decode(Ingredients.self, forKey: .ingredients)
        if let name = name {
            isLiked = UserDefaults.standard.object(forKey: name) as? Bool
        }
    }

    

}
