//
//  BeerManager.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 18.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import Foundation

class BeerManager: Codable {
    
    private static var beerManager: BeerManager?
    var beers: Array<Beer> = Array()
    var selectedBeer: Beer?
    
    static func getInstance() -> BeerManager? { // Singleton
        if beerManager == nil {
            beerManager = BeerManager.init()
        }
        return beerManager
    }
    
    func add(beer : Beer) {
        beers.append(beer)
        
    }
    
    func getFavourites() -> Array<Beer> {
        
        var favourites : Array<Beer> = Array()
        
        for i in 0 ... beers.count-1 {
            if beers[i].name != nil {
                if UserDefaults.standard.object(forKey: beers[i].name!) as? Bool ?? false {
                    if UserDefaults.standard.object(forKey: beers[i].name!) as! Bool{
                        favourites.append(beers[i])
                    }
                }
            }
        }
        return favourites
    }
    
    func numberOfBeers() -> Int {
        return beers.count
    }
    
    func get(num : Int) -> Beer {
        return beers[num]
    }
    
    func getSelectedBeer() -> Beer? {
        return selectedBeer
    }
    
    func setSelectedBeer(beer: Beer) {
        selectedBeer = beer
    }
    
    func getAll() -> Array<Beer> {
        return beers
    }
    
}
