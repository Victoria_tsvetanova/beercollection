//
//  SeachByResultsViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 19.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import AVFoundation

class SeachByResultsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var titleLabel: UINavigationItem!
    
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var noResultsLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var dateView: UIView!
    
    var popUpVC: PopUpViewController?
    
    var player: AVAudioPlayer!
    
    var allBeers = BeerManager.getInstance()
    
    var results: Array<Beer>?
    
    var searchBy: Criteria = Criteria.after
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noResultsLabel.isHidden = true
        titleLabel.title = searchBy.rawValue
        showTypeMethod()
        tableView.reloadData()
        
        makeSound()
        
        dateLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture))
        dateLabel.addGestureRecognizer(tapGesture)
        
        let nib = UINib(nibName: "BeerTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ReuseID")
        
    }
    
    @objc func tapGesture() {
        if searchBy.rawValue == "After" || searchBy.rawValue == "Before" {
            dateView.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toBeer" {
            let destinationVC = segue.destination as! BeerViewController
            destinationVC.setBeer(beer: allBeers!.getSelectedBeer()!)
        }
        
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        dateLabel.text = convertDate(date: datePicker.date)
        makeSound()
        dateView.isHidden = true
        reloadResults()
        
    }
    
    func reloadResults() {
        noResultsLabel.isHidden = true
        
        if let allBeers = allBeers {
            
            switch searchBy {
                case Criteria.byName:
                    results = allBeers.beers.filter({( beer : Beer) -> Bool in
                        return filterByName(beer: beer)
                    })
                case Criteria.after:
                    results = allBeers.beers.filter({( beer : Beer) -> Bool in
                        return filterAfter(date: beer.date)
                
                    })
                case Criteria.before:
                    results = allBeers.beers.filter({( beer : Beer) -> Bool in
                        return filterBefore(date: beer.date)
                    })
            case Criteria.food:
                results = allBeers.beers.filter({( beer : Beer) -> Bool in
                    return filterByFood(beer: beer)
                })
            }
        }
        
        if results?.count == 0 {
            self.noResultsLabel.isHidden = false
        }
        
        searchBar.resignFirstResponder()
        tableView.reloadData()
        
    }
    
    func convertDate(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        return dateFormatter.date(from: string)
        
    }
    
    func convertDate(date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
    
    func showTypeMethod() {
        if searchBy.rawValue == "By Name" || searchBy.rawValue == "Food" {
            searchBar.placeholder = searchBy.rawValue
            dateLabel.isHidden = true
            searchBar.isHidden = false
            dateView.isHidden = true
            searchBar.becomeFirstResponder()
            
        }
        else if searchBy.rawValue == "Before" || searchBy.rawValue == "After" {
            dateView.isHidden = false
            datePicker.isHidden = false
            searchBar.isHidden = true
        }
        
    }
    
    func makeSound() {
        let soundURL = Bundle.main.url(forResource: "PressButtonSound", withExtension: "mp3")
        
        do{
            if let soundURL = soundURL {
                try player =  AVAudioPlayer(contentsOf: soundURL)
            }
        }
        catch{
            print(error)
        }
        
        player.play()
    }
    
    //MARK: Button Pressed
    
    func didPressMore(beer: Beer) {
        makeSound()
        allBeers?.setSelectedBeer(beer: beer)
        performSegue(withIdentifier: "toBeer", sender: self)
        
    }
    
    func didPressPreview(beer: Beer) {
        makeSound()
        popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpID") as? PopUpViewController
        popUpVC?.showAnimate(beer: beer)
    }
    
    func didPressLike(cell: BeerTableViewCell) {
        makeSound()
        
        if cell.isLiked! {
            let image1 = UIImage(named: "heart_empty_icon")
            cell.likeImage.setImage(image1, for: .normal)
            cell.isLiked = false
        } else {
            let image1 = UIImage(named: "heart_full_icon")
            cell.likeImage.setImage(image1, for: .normal)
            cell.isLiked = true
        }
        
        cell.beer?.isLiked = cell.isLiked
        UserDefaults.standard.set(cell.isLiked, forKey: cell.nameLabel.text!)
        
    }
    
    //MARK: - Filters
    
    func filterByFood(beer: Beer) -> Bool{
        if beer.foodPairing != nil {
            for index in 0 ... beer.foodPairing!.count-1 {
                if beer.foodPairing![index].contains(searchBar.text!.lowercased()) {
                    return true
                }
            }
        }
        return false
        
    }
    
    func filterByName(beer: Beer) -> Bool {
        return beer.name?.lowercased().contains(searchBar.text!.lowercased()) ?? false
        
    }
    
    func filterAfter(date: String?) -> Bool{
        dateLabel.isHidden = false
        if convertDate(string: date!) == nil {
            return false
        }
        else {
            return convertDate(string: date!)! > datePicker.date
        }
        
    }
    
    func filterBefore(date: String?) -> Bool{
        dateLabel.isHidden = false
        if convertDate(string: date!) == nil {
            return false
        }
        else {
            return convertDate(string: date!)! < datePicker.date
        }
        
    }
    
}

//MARK: - Search bar methods

extension SeachByResultsViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        reloadResults()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBy.rawValue == "Before" {
            dateView.isHidden = false
        }
        else {
            dateView.isHidden = true
        }
    }

}

//MARK: - Table view methods

extension SeachByResultsViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let results = results {
            return results.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReuseID", for: indexPath) as? BeerTableViewCell {
            
            let item = results![indexPath.row]
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none //not clickable
            cell.setValues(beer: item)
            
            cell.didPressMore = didPressMore //not working ???
            cell.didPressPreview = didPressPreview
            cell.didPressLike = didPressLike
            
            return cell
        }
        else {
            fatalError("Cannot cast to BeerTableViewCell")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150
        
    }
 
    
}


