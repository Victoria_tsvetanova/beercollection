//
//  DeletePopUpViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 3.05.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class DeletePopUpViewController: UIViewController {
    
    var beer: Beer?
    
    var indexPath: IndexPath?
    
    var favVC: FavouritesViewController?
    
    var mainVC: MainViewController?
    
    var collectionCell: BeerCollectionViewCell?
    
    var beerVC: BeerViewController?
    
    var tableCell: BeerTableViewCell?
    
    var didPressRemove: ((Int) -> Void)?
    
    var didPressDislike: ((BeerCollectionViewCell) -> Void)?
    
    var answer = false
    
    @IBAction func noButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func yesButton(_ sender: Any) {
        if let indexPath = indexPath {
            favVC?.didPressRemove(indexPath: indexPath)
        }
        else if let cell = collectionCell {
            mainVC?.dislikeBeer(cell: cell)
        }
        else if let cell = tableCell {
            mainVC?.dislikeBeer(cell: cell)
        }
        else if let beer = beer {
            beerVC?.dislikeBeer(beer: beer)
        }
        removeAnimate()
        
    }
    
    @IBAction func backgroundTapped(_ sender: Any) {
        removeAnimate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func showAnimate() {
        
        let window2 = UIApplication.shared.keyWindow!
        let view = UIView(frame: window2.bounds)
        window2.addSubview(self.view)
        view.backgroundColor = UIColor.clear
        
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    


}
