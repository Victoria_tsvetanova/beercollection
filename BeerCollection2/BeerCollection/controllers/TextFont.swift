//
//  Font.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 3.05.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import Foundation

class TextFont {
    
    let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 16)
    
    let textFont = UIFont(name: "HelveticaNeue", size: 14)
    
    func getTitleFont() -> UIFont {
        return titleFont!
    }
    
    func getTextFont() -> UIFont {
        return textFont!
    }
    
}
