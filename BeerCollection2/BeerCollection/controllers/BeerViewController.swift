//
//  BeerViewController.swift
//  BeerCollection
//
//  Created by Victoria Tsvetanova on 14.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookShare

class HopCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
}

class BeerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedBeer: Beer?
    
    var hops: Array<Hop?> = Array()
    
    var isLiked = false
    
    var showBeerOfTheDay = false
    
    var deletePopUpVC: DeletePopUpViewController?
    
    @IBOutlet var beerOfTheDayLabel: UILabel!
    
    @IBOutlet var beerOfTheDayName: UILabel!
    
    @IBAction func beerOfTheDayOkButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBOutlet var beerOfTheDayLikeButton: UIButton!
    
    @IBOutlet var beerOfTheDayOkButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var beerImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UINavigationItem!
    
    @IBOutlet weak var beerDate: UILabel!
    
    @IBOutlet weak var hopsTableView: UITableView!
    
    @IBOutlet var favouriteButton: UIBarButtonItem!
    
    @IBAction func favouriteButtonPressed(_ sender: Any) {
        if self.isLiked {
            deletePopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deleteID") as? DeletePopUpViewController
            deletePopUpVC?.beerVC = self
            deletePopUpVC?.showAnimate()
            deletePopUpVC?.beer = selectedBeer
            
            
        } else {
            let likedImage = UIImage(named: "heart_full_icon")
            beerOfTheDayLikeButton.setImage(likedImage, for: .normal)
            favouriteButton.image = likedImage
            isLiked = true
        }
        
        selectedBeer?.isLiked = isLiked
        UserDefaults.standard.set(isLiked, forKey: nameLabel.title!)
        
    }
    
    func dislikeBeer(beer: Beer) {
        let notLikedImage = UIImage(named: "heart_empty_icon")
        favouriteButton.image = notLikedImage
        
        if showBeerOfTheDay {
            beerOfTheDayLikeButton.setImage(notLikedImage, for: .normal)
        }
        
        isLiked = false
        
        selectedBeer?.isLiked = isLiked
        UserDefaults.standard.set(isLiked, forKey: nameLabel.title!)
        
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        let imageURL = selectedBeer?.imageURL
        var content = LinkShareContent(url: NSURL(string: imageURL!)! as URL)
        if let name = selectedBeer?.name, let date = selectedBeer?.date {
            content.quote = "\(name) - \(date)"
        }
        let dialog = ShareDialog(content: content)
        dialog.presentingViewController = self
        
        do {
            try dialog.show()
        } catch (let error) {
            print("Error with sharing content: \(error)")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
        if showBeerOfTheDay == true {
            showLabels()
        }
    }
    
    func hideBeerOfTheDayLabels() {
        beerOfTheDayOkButton.isHidden = true
        beerOfTheDayName.isHidden = true
        beerOfTheDayLabel.isHidden = true
        beerOfTheDayLikeButton.isHidden = true
    }
    
    func showLabels(){
        beerOfTheDayOkButton.isHidden = false
        beerOfTheDayName.isHidden = false
        beerOfTheDayLabel.isHidden = false
        beerOfTheDayLikeButton.isHidden = false
    }
    
    func setValues() {
        
        hideBeerOfTheDayLabels()
        let font = TextFont()
        
        nameLabel.title = selectedBeer?.name
        
        beerOfTheDayName.text = selectedBeer?.name
        beerOfTheDayName.font = font.getTitleFont()
        
        beerDate.text = selectedBeer?.date
        beerDate.font = font.getTextFont()
        
        hops = (selectedBeer?.ingredients?.hops) ?? Array()
        isLiked = selectedBeer?.isLiked ?? false
        
        let isoDate = selectedBeer?.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.date(from:isoDate ?? "")
        
        if self.isLiked {
            let likedImage = UIImage(named: "heart_full_icon")
            favouriteButton.image = likedImage
            isLiked = true
        } else {
            let notLikedImage = UIImage(named: "heart_empty_icon")
            favouriteButton.image = notLikedImage
            isLiked = false
        }
        
        if let imageURL = selectedBeer!.imageURL,
            let url = URL(string: imageURL) {
            if let data = try? Data(contentsOf: url) {
                beerImageView.image = UIImage(data: data)
            }
        }
    }
    
    func setBeer(beer : Beer) {
        selectedBeer = beer
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (selectedBeer?.ingredients!.hops.count)!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HopCell", for: indexPath) as! HopCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
    
        let item = hops[indexPath.row]
        
        setCellValues(cell: cell, item: item!)
        
        return cell
        
    }
    
    func setCellValues(cell : HopCell, item : Hop){
        
        cell.nameLabel.text = item.name
        cell.amountLabel.text = ("\(item.amount!.value ?? 0) \("grams")")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 90
        
    }
    

}
