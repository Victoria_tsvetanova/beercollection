//
//  SearchViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 19.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import AVFoundation

enum Criteria: String{
    case byName = "By Name"
    case before = "Before"
    case after = "After"
    case food = "Food"
}

enum TypingStyle {
    case text
    case date
}

enum ButtonText: String {
    case edit = "Edit"
    case done = "Done"
}

class SearchViewController: UIViewController {
    
    var searchBy : Array<Criteria> = Array()
    
    var selectedSearchIndex : Int = -1
    
    var player: AVAudioPlayer!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var editButton: UIBarButtonItem!
  
    @IBAction func editButtonPressed(_ sender: Any) {
        if editButton.title == ButtonText.edit.rawValue {
            tableView.setEditing(true, animated: true)
            editButton.title = ButtonText.done.rawValue
        }
        else {
            tableView.setEditing(false, animated: true)
            editButton.title = ButtonText.edit.rawValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSearchBy()
        orderCriterias()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        makeSound()
    }
    
    func setSearchBy() {
        searchBy.append(Criteria.byName)
        searchBy.append(Criteria.before)
        searchBy.append(Criteria.after)
        searchBy.append(Criteria.food)
    }
    
    func orderCriterias() {
        for i in 0 ... searchBy.count-1 {
            let index = UserDefaults.standard.object(forKey: searchBy[i].rawValue) as? Int ?? -1
            if index != -1 {
                let temp = searchBy[index]
                searchBy[index] = searchBy[i]
                searchBy[i] = temp
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "searchBy" {
            let destinationVC = segue.destination as? SeachByResultsViewController
            destinationVC?.searchBy = searchBy[selectedSearchIndex]
        }
    }
    
    func makeSound() {
        let soundURL = Bundle.main.url(forResource: "PressButtonSound", withExtension: "mp3")
        
        do{
            if let soundURL = soundURL {
                try player =  AVAudioPlayer(contentsOf: soundURL)
            }
        }
        catch{
            print(error)
        }
        
        player.play()
    }
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.rowHeight = 150
        return searchBy.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as? SearchTableViewCell {
            
            
            
            let item = searchBy[indexPath.row]
            cell.searchTextLabel.text = item.rawValue
            return cell
        }
        else {
            fatalError("Cannot cast to SearchTableViewCell")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedSearchIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "searchBy", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let productToMove = searchBy[sourceIndexPath.row]
        searchBy.remove(at: sourceIndexPath.row)
        searchBy.insert(productToMove, at: destinationIndexPath.row )

        saveOrder()
        tableView.reloadData()
    }
    
    func saveOrder() {
        for i in 0 ... searchBy.count-1 {
            UserDefaults.standard.set(i, forKey: searchBy[i].rawValue)
        }
    }
    
   
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    

    
}
    
    



