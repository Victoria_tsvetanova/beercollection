//
//  FavouritesViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 17.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import AVFoundation

class FavouritesViewController: UIViewController {
    
    var allBeers = BeerManager.getInstance()
    
    var favBeers: Array<Beer>?
    
    var selectedBeer: Beer?
    
    var player: AVAudioPlayer!
    
    var popUpVC: PopUpViewController?
    
    var deletePopUpVC: DeletePopUpViewController?
    
    var indexPathToDelete: IndexPath?
    
    @IBOutlet weak var changeViewButton: UIBarButtonItem!
    
    @IBAction func changeViewButtonPressed(_ sender: Any) {
        didPressChangeViewButton()
        
    }
    
    @IBOutlet weak var tableView: UITableView!
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        tableView.reloadData()
        collectionView.isHidden = true
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.favBeers = (self.allBeers?.getFavourites())!
        }
        
        makeSound()
        tableView.reloadData()
        collectionView.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "favToBeer" {
                let destinationVC = segue.destination as! BeerViewController
                destinationVC.setBeer(beer: allBeers!.getSelectedBeer()!)
        }
    }
    
    func makeSound() {
        let soundURL = Bundle.main.url(forResource: "PressButtonSound", withExtension: "mp3")
        
        do{
            if let soundURL = soundURL {
                try player =  AVAudioPlayer(contentsOf: soundURL)
            }
        }
        catch{
            print(error)
        }
        
        player.play()
    }

    //MARK: Button actions
    
    func didPressMore(beer: Beer) {
        makeSound()
        allBeers?.setSelectedBeer(beer: beer)
        performSegue(withIdentifier: "favToBeer", sender: self)
        
    }
    
    func didPressPreview(beer: Beer) {
        makeSound()
        popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpID") as? PopUpViewController
        popUpVC?.showAnimate(beer: beer)
        tableView.allowsSelection = true
    }
    
    func didPressRemove(indexPath: IndexPath) {
        deletePopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deleteID") as? DeletePopUpViewController
        
       favBeers?[indexPath.row].isLiked = false
        if let favBeers = favBeers {
            UserDefaults.standard.set(false, forKey: favBeers[indexPath.row].name!)
        }
        favBeers?.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    
    }
    
    func didPressChangeViewButton() {
        makeSound()
        favBeers = allBeers?.getFavourites()
        if collectionView.isHidden == true {
            tableView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
            changeViewButton.image = UIImage(named: "icon_table")
        } else {
            tableView.isHidden = false
            tableView.reloadData()
            collectionView.isHidden = true
            changeViewButton.image = UIImage(named: "icon_collection")
        }
    }

}

// MARK: Table View

extension FavouritesViewController : UITableViewDelegate
, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favBeers?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.rowHeight = 150
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavouritesTableViewCell
        
        let item = favBeers?[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none //not clickable
        if let item = item {
            cell.setValues(beer: item)
        }
        
        cell.didPressMore = didPressMore
        
        cell.didPressPreview = didPressPreview
        
        cell.didPressRemove = didPressRemove
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func instanceFromNib() -> UIView {
        return UINib(nibName: "DeletePopUpView.xib", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            deletePopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deleteID") as?  DeletePopUpViewController
            
            deletePopUpVC?.indexPath = indexPath
            
            deletePopUpVC?.favVC = self
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! FavouritesTableViewCell
            
            let item = favBeers?[indexPath.row]
            if let item = item {
                cell.setValues(beer: item)
            }
            
            deletePopUpVC?.showAnimate()
            
        }
       
    }
    
    func updateData(at indexPath : IndexPath) {
        
        if let favBeers = favBeers {
            UserDefaults.standard.set(false, forKey: favBeers[indexPath.row].name!)
        }
        favBeers?.remove(at: indexPath.row)
        
    }
    
    
}

extension FavouritesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favBeers?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavCollectionCell", for: indexPath) as? FavouritesCollectionViewCell {
            
            let item = favBeers?[indexPath.row]
            if let item = item {
                cell.setValues(beer: item)
            }
            
            cell.didPressMore = self.didPressMore
            
            cell.didPressPreview = self.didPressPreview
            
            return cell
        }
        else {
            fatalError("Cannot cast to BeerCollectionViewCell")
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  collectionView.frame.size.width
        
        return CGSize(width: 160 + padding/20, height: 280)
    }
    
}
