//
//  PopUpViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 15.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import AVFoundation

class PopUpViewController: UIViewController {
    
    var beer: Beer?
    
    var player: AVAudioPlayer!
    
    @IBOutlet weak var popUpView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UITextView!
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func backgroundButton(_ sender: Any) {
        removeAnimate()
    }

    override func viewDidLoad() {
        nameLabel.adjustsFontSizeToFitWidth = true
        setValues(beer: beer!)
        super.viewDidLoad()
    }
    
    func setValues(beer: Beer){
        
        let font = TextFont()
        
        nameLabel.text = beer.name
        nameLabel.font = font.getTitleFont()
        
        dateLabel.text = beer.date
        dateLabel.font = font.getTextFont()
        
        descriptionLabel.text = beer.description
        descriptionLabel.font = font.getTextFont()
        
        if let imageURL = beer.imageURL,
            let url = URL(string: imageURL) {
            if let data = try? Data(contentsOf: url) {
                imageView.image = UIImage(data: data)
            }
        }
    }
    
    func showAnimate(beer: Beer) {
        
        self.beer = beer
        
        let window2 = UIApplication.shared.keyWindow!
        let view = UIView(frame: window2.bounds)
        window2.addSubview(self.view)
        view.backgroundColor = UIColor.clear
        
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
}
