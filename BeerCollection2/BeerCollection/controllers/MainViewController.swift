//
//  MainViewController.swift
//  BeerColection
//
//  Created by victoria.tsvetanova on 11.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import UserNotifications
import FacebookLogin
import FacebookShare
import AVFoundation

protocol ModalViewDelegator {
    func beerOfTheDay(beer: Beer)
}

class MainViewController: UIViewController {
    
    var firstForToday: Bool = false
    
    var player: AVAudioPlayer!
    
    let beerURL = "https://api.punkapi.com/v2/beers?page=2&per_page=80"
    
    var allBeers = BeerManager.getInstance()
    
    var delegate: BeerOfTheDayViewController?
    
    var deletePopUpVC: DeletePopUpViewController?
    
    var popUpViewController: PopUpViewController?
    
    var beerOfTheDay: Beer?
    
    var popUpVC : PopUpViewController!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var changeViewButton: UIBarButtonItem!
    
    @IBAction func changeViewButton(_ sender: Any) {
        didPressChangeViewButton()
       
    }
    
    @IBAction func favouritesButton(_ sender: Any) {
        makeSound()
        performSegue(withIdentifier: "goToFavourites", sender: self)
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        collectionView.isHidden = true
        
        setLastAccess()
        getBeerData()
        showNotification()
        
    }
    
    override func viewWillAppear(_ animated: Bool) { // refresh the main page
        tableView.reloadData()
        makeSound()
        
        let nib = UINib(nibName: "BeerTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ReuseID")
    }
    
    func getBeerData() {
    
        let url = URL(string: self.beerURL)
        URLSession.shared.dataTask(with: url!) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let data = try decoder.decode(Array<Beer>.self, from: data)
                DispatchQueue.main.async {
                    for index in 0 ... data.count-1 {
                        let beer = data[index]
                        self.allBeers?.add(beer: beer)
                    }
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
            } catch let err {
                print("Error", err)
            }
        }.resume()
        self.setBeerOfTheDay()
    }
    
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "fromMainToBeer" {
            let destinationVC = segue.destination as! BeerViewController
            destinationVC.setBeer(beer: allBeers!.getSelectedBeer()!)
        }
    
        if segue.identifier == "goToFavourites"{
            let destinationVC = segue.destination as? FavouritesViewController
            destinationVC?.allBeers = allBeers!
            destinationVC?.favBeers = allBeers!.getFavourites()
        }
        
    }
    
    func getRandomBeer() -> Beer? {
        setBeerOfTheDay()
        return beerOfTheDay
        
    }
    
    func makeSound() {
        let soundURL = Bundle.main.url(forResource: "PressButtonSound", withExtension: "mp3")
        
        do{
            if let soundURL = soundURL {
                try player =  AVAudioPlayer(contentsOf: soundURL)
            }
        }
        catch{
            print(error)
        }
        
        player.play()
    }
    
    //MARK: Notifications
    
    @objc func scheduleLocal() {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content1 = setContent(title: "Check our selection of beers🧐", body: "Hey, buddy, what’s up? Would you like to check \(getRandomBeer()?.name ?? "our new beer")🍺- one of the top beers of 2018. See its ingredients and maybe click the ❤️ button ?", categoryIdentifier: "alarm")
        let content2 = setContent(title: "Come back. We miss you already", body: "Hey, what's up? 😄 Click here to go back to our app and check all of the beers we would like to present to you. 🍻", categoryIdentifier: "alarm")
        let trigger1 = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
        let trigger2 = UNTimeIntervalNotificationTrigger(timeInterval: 3600, repeats: true)
        
        let request1 = UNNotificationRequest(identifier: UUID().uuidString, content: content1, trigger: trigger1)
        let request2 = UNNotificationRequest(identifier: UUID().uuidString, content: content2, trigger: trigger2)
        center.add(request1)
        center.add(request2)
    }
    
    func setContent(title: String, body: String, categoryIdentifier: String) -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.categoryIdentifier = categoryIdentifier
        content.sound = .default
        return content
    }
    
    func showNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(scheduleLocal), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    
    //MARK: Button actions
    
    func didPressMore(beer: Beer) {
        makeSound()
        allBeers?.setSelectedBeer(beer: beer)
        performSegue(withIdentifier: "fromMainToBeer", sender: self)
        
    }
    
    func didPressPreview(beer: Beer) {
        makeSound()
        popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popUpID") as? PopUpViewController
        popUpVC?.showAnimate(beer: beer)
        
    }
    
    func didPressLike(cell: BeerTableViewCell) { // TABLE CELL LIKE
        makeSound()
        
        if cell.isLiked! {
            deletePopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deleteID") as? DeletePopUpViewController
            deletePopUpVC?.mainVC = self
            deletePopUpVC?.tableCell = cell
            deletePopUpVC?.showAnimate()
            
        } else {
            let image1 = UIImage(named: "heart_full_icon")
            cell.likeImage.setImage(image1, for: .normal)
            cell.isLiked = true
        }

        cell.beer?.isLiked = cell.isLiked
        UserDefaults.standard.set(cell.isLiked, forKey: cell.nameLabel.text!)
        
    }
    
    func dislikeBeer(cell: BeerTableViewCell) {
        let image1 = UIImage(named: "heart_empty_icon")
        cell.likeImage.setImage(image1, for: .normal)
        cell.isLiked = false
        UserDefaults.standard.set(cell.isLiked, forKey: cell.nameLabel.text!)
    }

    
    
    func didPressLike(cell: BeerCollectionViewCell) { // COLLECTION CELL
        makeSound()
        
        if cell.isLiked! {
            deletePopUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deleteID") as? DeletePopUpViewController
            deletePopUpVC?.mainVC = self
            deletePopUpVC?.collectionCell = cell
            deletePopUpVC?.showAnimate()
            
        } else {
            let image1 = UIImage(named: "heart_full_icon")
            cell.likeImage.setImage(image1, for: .normal)
            cell.isLiked = true
        }
        
        cell.beer?.isLiked = cell.isLiked
        UserDefaults.standard.set(cell.isLiked, forKey: cell.nameLabel.text!)
        
    }
    
    func dislikeBeer(cell: BeerCollectionViewCell) {
        let image1 = UIImage(named: "heart_empty_icon")
        cell.likeImage.setImage(image1, for: .normal)
        cell.isLiked = false
        UserDefaults.standard.set(cell.isLiked, forKey: cell.nameLabel.text!)
    }
    
    func didPressChangeViewButton() {
        makeSound()
        
        if collectionView.isHidden == true {
            tableView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
            changeViewButton.image = UIImage(named: "icon_table")
        } else {
            tableView.isHidden = false
            tableView.reloadData()
            collectionView.isHidden = true
            changeViewButton.image = UIImage(named: "icon_collection")
        }
    }
    
    //MARK: Beer Of The Day
    
   // let dayInSec : Double = 86400 // 24h in sec
    let dayInSec: Double = 20 // test
    
    func setLastAccess() {
        let now = Date().timeIntervalSinceReferenceDate
        let lastAccess = (UserDefaults.standard.object(forKey: "lastAccess") as? Double) ?? (now - dayInSec)
        if now >= (lastAccess + dayInSec) {
            firstForToday = true
            UserDefaults.standard.set(now, forKey: "lastAccess")
        }
        else {
            firstForToday = false
        }
    }
    
    func showBeerOfTheDay() {
        if firstForToday {
            showModal()
        }
        
    }
    
    func showModal() {
        let modalViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "beerID") as? BeerViewController
        modalViewController?.selectedBeer = beerOfTheDay
        modalViewController?.modalPresentationStyle = .overFullScreen
        modalViewController?.showBeerOfTheDay = true
        
        present(modalViewController!, animated: true, completion: nil)
    
    }
    
    func setBeerOfTheDay() {
        
        let url = URL(string: "https://api.punkapi.com/v2/beers/random")
        URLSession.shared.dataTask(with: url!) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let data = try decoder.decode([Beer].self, from: data)
                self.beerOfTheDay = data[0]
                DispatchQueue.main.async { // by main thread
                    self.showBeerOfTheDay()
                }
            } catch let err {
                print("Error", err)
            }
            }.resume()
        
    }

}

//MARK: Collection View

extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allBeers?.numberOfBeers() ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? BeerCollectionViewCell {
            
            let item = allBeers?.get(num: indexPath.item)
            
            cell.setValues(beer: item!)
            
            cell.didPressMore = self.didPressMore
            
            cell.didPressLike = self.didPressLike
            
            cell.didPressPreview = self.didPressPreview
            
            return cell
        }
        else {
            fatalError("Cannot cast to BeerCollectionViewCell")
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  collectionView.frame.size.width
        
        return CGSize(width: 160 + padding/20, height: 280)
    }
}

//MARK: Table View

extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allBeers?.numberOfBeers() ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReuseID", for: indexPath) as? BeerTableViewCell {
        
            let item = allBeers?.get(num: indexPath.row)
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none //not clickable
            
            cell.setValues(beer: item!)
            
            cell.didPressMore = self.didPressMore

            cell.didPressLike = self.didPressLike

            cell.didPressPreview = self.didPressPreview
            
            return cell
        }
        else {
            fatalError("Cannot cast to BeerTableViewCell")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150
        
    }
    
}

