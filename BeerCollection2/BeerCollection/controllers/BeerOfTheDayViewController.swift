//
//  BeerOfTheDayViewController.swift
//  BeerCollection
//
//  Created by victoria.tsvetanova on 24.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit


class BeerOfTheDayViewController: UIViewController, ModalViewDelegator {
    
    func beerOfTheDay(beer: Beer) {
        selectedBeer = beer
        show()
    }
    
    var showBeerOfTheDay: ((Beer) -> Void)?
    
    var selectedBeer: Beer?
    
    var isLiked: Bool?
    
    var hops: Array<Hop?>?
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var likeButton: UIButton!
    
    @IBAction func likeButton(_ sender: Any) {
        if isLiked != nil {
            if self.isLiked! {
                let notLikedImage = UIImage(named: "heart_empty_icon")
                likeButton.setImage(notLikedImage, for: .normal)
                isLiked = false
            } else {
                let likedImage = UIImage(named: "heart_full_icon")
                likeButton.setImage(likedImage, for: .normal)
                isLiked = true
            }
            selectedBeer?.isLiked = isLiked
            UserDefaults.standard.set(isLiked, forKey: nameLabel.text!)
        }
        
        
    }
    
    @IBOutlet var descriptionTextView: UITextView!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var tableView: UITableView!
    
    @IBAction func okButton(_ sender: Any) {
        removeAnimate()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()

    }
    
    func setValues() {
        let font = TextFont()
        
        nameLabel.text = selectedBeer?.name
        nameLabel.font = font.getTitleFont()
        
        dateLabel.text = selectedBeer?.date
        dateLabel.font = font.getTextFont()
        
        descriptionTextView.text = selectedBeer?.description
        descriptionTextView.font = font.getTextFont()
        
        hops = (selectedBeer?.ingredients?.hops) ?? Array()
        isLiked = selectedBeer?.isLiked ?? false
        
        let isoDate = selectedBeer?.date ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.date(from:isoDate)
        
        if isLiked != nil {
            if isLiked! {
                let image1 = UIImage(named: "heart_full_icon")
                likeButton.setImage(image1, for: .normal)
            } else {
                let image1 = UIImage(named: "heart_empty_icon")
                likeButton.setImage(image1, for: .normal)
            }
        }
            
        if let imageURL = selectedBeer!.imageURL,
            let url = URL(string: imageURL) {
            if let data = try? Data(contentsOf: url) {
                imageView.image = UIImage(data: data)
            }
        }
        
        
    }
    
    func show() {
        
        let window2 = UIApplication.shared.keyWindow!
        let view = UIView(frame: window2.bounds)
        window2.addSubview(self.view)
        view.backgroundColor = UIColor.clear
        
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}

//MARK: TableView

extension BeerOfTheDayViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tableView.rowHeight = 90
        
        return selectedBeer?.ingredients?.hops.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HopCell", for: indexPath) as! HopCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let item = hops?[indexPath.row]
        
        setCellValues(cell: cell, item: item!)
        
        return cell
        
    }
    
    func setCellValues(cell : HopCell, item : Hop){
        cell.nameLabel.text = item.name
        cell.amountLabel.text = ("\(item.amount?.value ?? 0) \("grams")")
        
    }
    
}
