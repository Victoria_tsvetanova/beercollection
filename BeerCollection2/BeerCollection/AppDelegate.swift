//
//  AppDelegate.swift
//  BeerColection
//
//  Created by victoria.tsvetanova on 11.04.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
       
        self.saveContext()
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        registerLocal()
    }
    
    
    @objc func registerLocal() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { granted,
            error in
            if granted {
                print("Got the access")
            } else {
                print("Access denied")
            }
        })
    }
    

    
    func getRandomBeer() -> Beer? {
        
        var ranBeer: Beer?
        let url = URL(string: "https://api.punkapi.com/v2/beers/random")
        URLSession.shared.dataTask(with: url!) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let data = try decoder.decode([Beer].self, from: data)
                ranBeer = data[0]
            } catch let err {
                print("Error", err)
            }
            }.resume()
        
        return ranBeer
    }

    lazy var persistentContainer: NSPersistentContainer = {
    
        let container = NSPersistentContainer(name: "BeerColection")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

